﻿namespace AdsPlayer
{
    partial class CodeGenerator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnstop = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_process = new System.Windows.Forms.Button();
            this.txt_code = new System.Windows.Forms.TextBox();
            this.btn_oldcode = new System.Windows.Forms.Button();
            this.btn_getnewcode = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btn_moveforward = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_moveforward);
            this.panel1.Controls.Add(this.btnstop);
            this.panel1.Controls.Add(this.progressBar1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btn_process);
            this.panel1.Controls.Add(this.txt_code);
            this.panel1.Controls.Add(this.btn_oldcode);
            this.panel1.Controls.Add(this.btn_getnewcode);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(471, 300);
            this.panel1.TabIndex = 0;
            // 
            // btnstop
            // 
            this.btnstop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnstop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnstop.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnstop.Location = new System.Drawing.Point(420, 248);
            this.btnstop.Name = "btnstop";
            this.btnstop.Size = new System.Drawing.Size(48, 23);
            this.btnstop.TabIndex = 11;
            this.btnstop.Text = "Stop Process";
            this.btnstop.UseVisualStyleBackColor = false;
            this.btnstop.Visible = false;
            this.btnstop.Click += new System.EventHandler(this.button1_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(3, 274);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(465, 23);
            this.progressBar1.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 258);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(10, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = ".";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(101, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Code:";
            // 
            // btn_process
            // 
            this.btn_process.Enabled = false;
            this.btn_process.Location = new System.Drawing.Point(104, 128);
            this.btn_process.Name = "btn_process";
            this.btn_process.Size = new System.Drawing.Size(254, 36);
            this.btn_process.TabIndex = 6;
            this.btn_process.Text = "Check Activation";
            this.btn_process.UseVisualStyleBackColor = true;
            this.btn_process.Click += new System.EventHandler(this.btn_process_Click);
            // 
            // txt_code
            // 
            this.txt_code.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_code.Enabled = false;
            this.txt_code.Location = new System.Drawing.Point(104, 60);
            this.txt_code.Name = "txt_code";
            this.txt_code.Size = new System.Drawing.Size(254, 20);
            this.txt_code.TabIndex = 5;
            // 
            // btn_oldcode
            // 
            this.btn_oldcode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_oldcode.Location = new System.Drawing.Point(237, 86);
            this.btn_oldcode.Name = "btn_oldcode";
            this.btn_oldcode.Size = new System.Drawing.Size(121, 36);
            this.btn_oldcode.TabIndex = 4;
            this.btn_oldcode.Text = "Generate Old Code";
            this.btn_oldcode.UseVisualStyleBackColor = true;
            this.btn_oldcode.Click += new System.EventHandler(this.btn_oldcode_Click);
            // 
            // btn_getnewcode
            // 
            this.btn_getnewcode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_getnewcode.Location = new System.Drawing.Point(104, 86);
            this.btn_getnewcode.Name = "btn_getnewcode";
            this.btn_getnewcode.Size = new System.Drawing.Size(121, 36);
            this.btn_getnewcode.TabIndex = 3;
            this.btn_getnewcode.Text = "Generate New Code";
            this.btn_getnewcode.UseVisualStyleBackColor = true;
            this.btn_getnewcode.Click += new System.EventHandler(this.btn_getnewcode_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btn_moveforward
            // 
            this.btn_moveforward.Enabled = false;
            this.btn_moveforward.Location = new System.Drawing.Point(104, 170);
            this.btn_moveforward.Name = "btn_moveforward";
            this.btn_moveforward.Size = new System.Drawing.Size(254, 36);
            this.btn_moveforward.TabIndex = 12;
            this.btn_moveforward.Text = "Process";
            this.btn_moveforward.UseVisualStyleBackColor = true;
            this.btn_moveforward.Click += new System.EventHandler(this.btn_moveforward_Click);
            // 
            // CodeGenerator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 300);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CodeGenerator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CodeGenerator";
            this.Load += new System.EventHandler(this.CodeGenerator_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txt_code;
        private System.Windows.Forms.Button btn_oldcode;
        private System.Windows.Forms.Button btn_getnewcode;
        private System.Windows.Forms.Button btn_process;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button btnstop;
        private System.Windows.Forms.Button btn_moveforward;
    }
}

