﻿using AdsPlayer.Common;
using AdsPlayer.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdsPlayer
{
    public partial class CodeGenerator : Form
    {
        Thread thread;
        PlayAds sx = new PlayAds();
       
        public CodeGenerator()
        {
            InitializeComponent();
            GetDefaultConstant();
        }
        #region Method/Function
        public void RunBar()
        {

            timer1.Start();
        }
        public string GetRegisterId()
        {
            //API for Fetching UID  
            try
            {
                txt_code.Text = string.Empty;
                HttpClient client = new HttpClient();
                var urlProfile = Constants.WebAPI_Register;
                var httpRequestMessage = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri(urlProfile),
                    Headers =
                    {
                       { HttpRequestHeader.Accept.ToString(), "application/json" } //  response data type assigment 
                    }
                };
                var response = client.SendAsync(httpRequestMessage).Result;
                if (response.IsSuccessStatusCode)
                {
                    string outputData = response.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<RootRegister>(outputData); //  Deserialize the json object to a C# class
                    return result.uid;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return string.Empty;
            }
        }
        public bool GetActivationResponse()
        { //Get
            string Uid = string.IsNullOrEmpty(txt_code.Text) ? "d1Ij" : txt_code.Text;
            try
            {

                HttpClient client = new HttpClient();

                var urlProfile = $"{Constants.WEBAPI_ActivationResponse}?uid={Uid}";
                var httpRequestMessage = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri(urlProfile),
                    Headers =
                    {
                       { HttpRequestHeader.Accept.ToString(), "application/json" }
                    }
                };
                var response = client.SendAsync(httpRequestMessage).Result;
                if (response.IsSuccessStatusCode)
                {
                    string outputData = response.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ActivetedUidResponse>(outputData);
                    bool Response = result.result;
                    return Response;
                }
                else
                {

                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public void RunProcessing()
        {
            while (true)
            {
                bool ReturnRepsonce = GetActivationResponse();
                if (ReturnRepsonce)
                {
               
                    TextBox.CheckForIllegalCrossThreadCalls = false;
                    Form.CheckForIllegalCrossThreadCalls = false;
                    Properties.Settings.Default.Uid = txt_code.Text;
                    btn_moveforward.Enabled = true;
                    btnstop.Visible = false;
                    btn_process.Enabled = false;
                    timer1.Stop();
                    progressBar1.Value = 0;
                    label3.Text = ".";
                    break;

                }

            }
            
    
        }
        public void GetNewCode()
        {
            string Uid = "";
            while (true)
            {
                Uid = GetRegisterId();
                if (!string.IsNullOrWhiteSpace(Uid))
                {
                  
                    TextBox.CheckForIllegalCrossThreadCalls = false;
                    txt_code.Text = Uid;
                    btn_process.Enabled = true;
                    timer1.Stop();
                    progressBar1.Value = 0;
                    label3.Text = ".";
                    break; 
                }
            }
          
        }

        #endregion
        #region Form Events
        private void btn_getnewcode_Click(object sender, EventArgs e)
        {
            RunBar();
            thread = new Thread(() => GetNewCode());
            thread.IsBackground = true;
            thread.Start();            
        }
        private void btn_oldcode_Click(object sender, EventArgs e)
        {
            txt_code.Enabled = true;
            btn_process.Enabled = true;

        }
        private void btn_process_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt_code.Text))
            {
                MessageBox.Show("Please Enter code");
                return;
            }
            else if (txt_code.TextLength < 4)
            {
                MessageBox.Show("Please Enter Right Formate Code that is not less then 4");
                txt_code.Clear();
                return;
            }
            btnstop.Visible = true;
            RunBar();
            thread = new Thread(() => RunProcessing());
            thread.IsBackground = true;
            thread.Start();

        }
        private void CodeGenerator_Load(object sender, EventArgs e)
        {

        }
        private void timer1_Tick(object sender, EventArgs e)
        {

            try
            {
                progressBar1.Visible = true;
                progressBar1.Value = progressBar1.Value + 2;
                if ((progressBar1.Value == 10))
                {
                    label3.Text = "Ready To Check Web API Server";
                }
                else if ((progressBar1.Value == 20))
                {
                  //  label3.Text = "Turning on modules-.";
                }
                else if ((progressBar1.Value == 40))
                {
                    label3.Text = "Checking Api Response....";
                }
                else if ((progressBar1.Value == 80))
                {
                    //label3.Text = "Done Loading modules..";
                }
                else if ((progressBar1.Value == 100))
                {
                    label3.Text = "Starting Again";
                    progressBar1.Value = 0;

                }


            }
            catch (Exception ex)
            {

                System.Environment.Exit(0);
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            thread.Abort();
            btnstop.Visible = false;
            label3.Text = ".";
            progressBar1.Value = 0;
            timer1.Stop();
        }
        #endregion
        public void GetDefaultConstant()
        {
            try
            {
                List<Result> objlist = new List<Result>();
                HttpClient client = new HttpClient();

                var urlProfile = $"{Constants.WEBAPI_Constanst}";
                var httpRequestMessage = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri(urlProfile),
                    Headers =
                    {
                       { HttpRequestHeader.Accept.ToString(), "application/json" }
                    }
                };
                var response = client.SendAsync(httpRequestMessage).Result;
                if (response.IsSuccessStatusCode)
                {
                    string outputData = response.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<RootConstant>(outputData);

                    foreach (var item in result.result)
                    {
                        Result obj = new Result();

                        if (item.name.Contains("DEFAULT_AD"))
                        {
                            obj.name = item.name;
                            obj.values = item.values;
                            objlist.Add(obj);
                        }
                        else if (item.name.Contains("DEFAULT_AD_TYPE"))
                            {
                            obj.name = item.name;
                            obj.values = item.values;
                            objlist.Add(obj);
                        }



                    }

                     StaticConstant.ConstantList = objlist;
                    //sx.Show();
                   
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                
            }
        }

        private void btn_moveforward_Click(object sender, EventArgs e)
        {
            this.Hide();
            sx.Show();
        }
    }

}
