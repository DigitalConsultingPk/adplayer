﻿using AdsPlayer.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdsPlayer.Common
{
    public class APIs
    {
        public void GetAds()
        {
            try
            {
                List<File_api> objlist = new List<File_api>();
                HttpClient client = new HttpClient();

                var urlProfile = Constants.WEBAPI_Test;
                var httpRequestMessage = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri(urlProfile),
                    Headers =
                    {
                       { HttpRequestHeader.Accept.ToString(), "application/json" }
                    }
                };
                var response = client.SendAsync(httpRequestMessage).Result;
                if (response.IsSuccessStatusCode)
                {
                    string outputData = response.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<List<Root_Ads>>(outputData);
                    var playlist = result.Select(x => x.campaign).ToList().Select(x => x.playlist).ToList();
                    foreach (var item in playlist)
                    {
                        foreach (var x in item)
                        {
                            var adsfile = x.file;
                            adsfile.duration = x.duration;
                            objlist.Add(adsfile);
                        }
                    }
                    StaticConstant.ListAds = objlist;


                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
    }
}
