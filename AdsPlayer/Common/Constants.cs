﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdsPlayer.Common
{
    public static partial class Constants
    {
        public const string WebAPI_Register = "https://api.kimikads.com/api/displays/register/";
        public const string WEBAPI_ActivationResponse = "https://api.kimikads.com/api/displays/is_active/";
        public const string WEBAPI_Constanst = "https://api.kimikads.com/api/constants";
        public const string WEBAPI_AdsPlay = "https://api.kimikads.com/api/displays/get_ads/?uid=";
        public const string WEBAPI_Test = "https://kimikads-api.onpy.ru/api/displays/get_playlist/?uid=Vg5j&accept_urls";

    }
}
