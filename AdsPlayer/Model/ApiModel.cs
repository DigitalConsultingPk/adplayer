﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdsPlayer.Model
{
    #region Register

    #endregion
    public class RootRegister
    {
        //https://api.kimikads.com/api/displays/register/ (POST API)
        public string uid { get; set; }
    }

    #region Activation

    #endregion
    public class ActivetedUidResponse
    {
        //https://api.kimikads.com/api/displays/is_active/?uid=d1Ij (Get API)
        public bool result { get; set; }
    }
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    #region Constant

    #endregion

    public class Result
    {
        public string name { get; set; }
        public object values { get; set; }
    }

    public class RootConstant
    {
        public List<Result> result { get; set; }
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class File_api
    {
        public string id { get; set; }
        public string type { get; set; }
        public string type_str { get; set; }
        public string link { get; set; }
        public string preview { get; set; }
        public string duration { get; set; }
        public string img { get; set; }
        public string filename { get; set; }
    }

    public class Playlist
    {
        public string uid { get; set; }
        public string duration { get; set; }
        public File_api file { get; set; }
    }

    public class Campaign
    {
        public string id { get; set; }
        public string type { get; set; }
        public List<Playlist> playlist { get; set; }
    }

    public class Root_Ads
    {
        public string id { get; set; }
        public Campaign campaign { get; set; }
        public string schedule { get; set; }
    }

    public class Ads_File
    {
        public string id { get; set; }
        public string type { get; set; }
        public string type_str { get; set; }
        public string link { get; set; }
        public string preview { get; set; }
        public string duration { get; set; }
        public string img { get; set; }
        public string filename { get; set; }
    }
}
