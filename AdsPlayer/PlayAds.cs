﻿using AdsPlayer.Common;
using AdsPlayer.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdsPlayer
{
    public partial class PlayAds : Form
    {
        Thread thread;

        public PlayAds()
        {
            InitializeComponent();
        }

        private void PlayAds_Load(object sender, EventArgs e)
        {
            var list = StaticConstant.ConstantList;

            if (list==null)
            {

            }
            //foreach (var item in list)
            //{
            //    axWindowsMediaPlayer1.uiMode = "None";
            //    axWindowsMediaPlayer1.URL = item.values.ToString();
            //    axWindowsMediaPlayer1.settings.setMode("loop", true);
            //    break;
            //}
            thread = new Thread(() => GetAds());
            thread.IsBackground = true;
            thread.Start();
            while(true)
            {
                if(thread.IsAlive)
                {

                }
                else
                {
                    var a = StaticConstant.ListAds;
                    break;
                }
            }


        }

        public void  GetAds()
        {
            try
            {
                List<File_api> objlist = new List<File_api>();
                HttpClient client = new HttpClient();

                var urlProfile = Constants.WEBAPI_Test;
                var httpRequestMessage = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri(urlProfile),
                    Headers =
                    {
                       { HttpRequestHeader.Accept.ToString(), "application/json" }
                    }
                };
                var response = client.SendAsync(httpRequestMessage).Result;
                if (response.IsSuccessStatusCode)
                {
                    string outputData = response.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<List<Root_Ads>>(outputData);
                    var playlist = result.Select(x => x.campaign).ToList().Select(x => x.playlist).ToList();
                    foreach (var item in playlist)
                    {
                        foreach (var x in item)
                        {
                            var adsfile = x.file;
                            adsfile.duration = x.duration;
                            objlist.Add(adsfile);
                        }
                    }
                    StaticConstant.ListAds = objlist;


                }
              

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
              
            }
        }

        /*
            webBrowser1.Visible = false;
            // webBrowser1.Navigate(@"https://www.youtube.com/watch?v=sBAAgrQBCxc");
            axWindowsMediaPlayer1.uiMode = "None";
            //  axWindowsMediaPlayer1.URL = @"https://s3.amazonaws.com/stipio.com/media/campaign_file/6gSzDl6XOY.mp4";

            axWindowsMediaPlayer1.URL = @"https://api.kimikads.com/media/k-espec.jpg";
            //axWindowsMediaPlayer1.URL = @"https://www.wwe.com/";
            axWindowsMediaPlayer1.settings.setMode("loop", true);
         */
    }
}
